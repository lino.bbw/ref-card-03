# Architecture Ref. Card 03

Ref-card-03 ist eine Spring Boot Web-Aplikation mit einer MariaDB Datenbank welche Witze anzeigt

## Vorausetzungen
Das Projekt basiert auf Docker welches installiert werden muss. [Anleitung Install Docker](https://docs.docker.com/engine/install/)

**Windows**
1. WSL installieren: [Installieren von WSL Microsoft Learn](https://learn.microsoft.com/en-us/windows/wsl/install)
```sh
wsl --install
```
2. Docker Desktop herunterladen [Docker Desktop Installer.exe](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe) und die Installation ausführen


## Installation und Verwendung
Projekt herunterladen

```sh
git clone https://gitlab.com/bbwrl/m346-ref-card-03.git
cd m346-ref-card-03
```

Builden des Docker Containers
```sh
docker build -t ref-card-03:latest
```

Docker Images ansehen
```sh
docker images

Docker Container ausführen mit Port
```sh
docker rn -p 8080:8080 ref-card-03:latest
```

## Datenbank
Netzwerk erstellen
```sh
docker network create some-network 
```

MariaDB starten
```sh
docker run --detach --network some-network --name mariadb-ref-card-03 --env MARIADB_USER=jokedbuser --env MARIADB_PASSWORD=123456 --env MARIADB_ROOT_PASSWORD=123456 -v ./src/resources/:/docker-entrypoint-initdb.d/ mariadb:latest
```

Im Browser ist die App unter der URL http://localhost:8080 erreichbar.